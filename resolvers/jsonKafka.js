const { v4: uuidv4 } = require('uuid');
const { Kafka } = require('kafkajs');

const Queries = {
    kafkaReceive: async (args, req) => {

        let current_datetime = new Date()
        let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds() 

        messageJson = { 
            id: uuidv4(),
            date: formatted_date,
            name: args.name,
            surname: args.surname,
            age: args.age,
            answer_1: args.answer_1,
            answer_2: args.answer_2,
            answer_3: args.answer_3,
            amount: args.amount,
            rating: args.rating           
        }
    
        messageString = JSON.stringify(messageJson)
        const messages = [{value: messageString}] 

        kafkaClient = new Kafka({
            clientId: "my-app",
            brokers: ["localhost:9092"]
        });

        const producer = kafkaClient.producer();
        const topic = "test_topic_2";

        const sendMessage = async () => {   

            message = {
                topic: topic,
                messages: messages
            }
        
            await producer.connect()
            await producer.send(message)
            await producer.disconnect()
        }

        sendMessage(producer, topic);

    }
}

module.exports = Queries

