const avro = require('avsc');

const Queries = {
    avroTemplate: async (args, req) => {

        let current_datetime = new Date()
        let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds() 
       

        const type = avro.Type.forSchema({
            type: 'record',
            fields: [
              {name: 'name', type: 'string'},
              {name: 'surname', type: 'string'}
            ]
          });
           
          const buf = type.toBuffer({name: 'Tom', surname: 'Kotowski'}); 
          const val = type.fromBuffer(buf); 

          console.log(buf);
          console.log(val);


    }
}

module.exports = Queries

