const { v4: uuidv4 } = require('uuid');
const aws = require('aws-sdk');


const Queries = {
    uploadFile: async(args, req) => {

        let current_datetime = new Date()
        let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds() 
  
        const fileType = 'json'

        messageJson = { 
            id: uuidv4(),
            date: formatted_date,
            name:  args.name,
            surname: args.surname,
            age: args.age        
        }

        const s3 = new aws.S3({
            accessKeyId: process.env.AWS_ACCESS_ID,
            secretAccessKey: process.env.SECRET_AWS_ACCESS_ID
        })

        const params = {
            Bucket: process.env.AWS_BUCKET_NAME,
            Key: `${messageJson.id}.${fileType}`,
            Body: JSON.stringify(messageJson),
            ContentType: "application/json"
        };

        // s3.putObject(params,
        //     function (err,data) {
        //         console.log(err, data);
        //     }
        // );  

        s3.upload(params, 
            function(err, data) {
                console.log(err, data);
            }
        );  

    }
}

module.exports = Queries

