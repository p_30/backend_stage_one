const defaultResolver = require('./default_actions');
const s3Resolver = require('./jsonS3');
const kafkaResolver = require('./jsonKafka');
const avroResolver = require('./avroTemplate');


const rootResolver = {
  ...defaultResolver,
  ...s3Resolver,
  ...kafkaResolver,
  ...avroResolver
};

module.exports = rootResolver;
