require('dotenv/config');
const express = require('express');
const bodyParser = require('body-parser');
const graphQlSchema = require('./schema/index');
const graphQlResolvers = require('./resolvers/index');
const graphqlHttp = require('express-graphql');


const app = express();
const port = process.env.APP_PORT;



app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, AuthorizationRefresh');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});

app.use('/graphql', graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
    })
);


app.listen(port);


