const graphql = require('graphql');


const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull } = graphql;


const OutputType = new GraphQLObjectType({
    name: 'Output',
    fields: ( ) => ({
        code: { type: GraphQLString }
    })
});


const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        defaultQuery: {
            type: OutputType,
        }
    }
});


const RootMutation = new GraphQLObjectType({
    name: 'RootMutationType',
    fields: {
        defaultMutation: {
            type: OutputType,        
        },
        uploadFile: {
            type: OutputType,
            args: {
                name: { type: GraphQLString },
                surname: { type: GraphQLString },
                age: { type: GraphQLInt }
            }
        },
        kafkaReceive: {
            type: OutputType,
            args: { 
                answer_1: { type: GraphQLString },
                answer_2: { type: GraphQLString },
                answer_3: { type: GraphQLString },
                amount: { type: GraphQLInt },
                rating: { type: GraphQLInt },                
                name: { type: GraphQLString },
                surname: { type: GraphQLString },
                age: { type: GraphQLInt }
            }
        },
        avroTemplate: {
            type: OutputType,
        }
    }
});


module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation
});


